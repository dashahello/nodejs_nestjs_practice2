import { NestFactory } from '@nestjs/core';
import * as fs from 'fs';

import { ValidationPipe } from '@common/pipes/validation.pipe';

import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.setGlobalPrefix('api');
  app.useGlobalPipes(new ValidationPipe());

  fs.writeFileSync(process.env.LOG_FILE, '');

  await app.listen(process.env.APP_PORT);
}
bootstrap();
