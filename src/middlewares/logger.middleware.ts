import { Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
import * as fs from 'fs';

@Injectable()
export class Logger implements NestMiddleware {
  use(request: Request, response: Response, next: NextFunction) {
    const dateAndTime = new Date().toLocaleString('en-US');
    const requestUrl =
      request.protocol + '://' + request.get('host') + request.originalUrl;

    const entry = `date and time: ${dateAndTime}\n
    method: ${request.method}\n
    url: ${requestUrl}\n
    user agent: ${request.get('User-Agent')}\n
    ip:${request.ip}\n
    - - - - - - - - - - - - - - - - - - \n\n`;

    console.log('Request info: ', entry);

    fs.appendFile(process.env.LOG_FILE, entry, () => {});

    next();
  }
}
