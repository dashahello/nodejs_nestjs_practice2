import { Injectable } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';

import { tokens } from '../tokens';

@Injectable()
export class TokenExpirationService {
  @Cron('60 * * * * *')
  handleCron() {
    const now = Date.now();

    for (let i = 0; i < tokens.length; i++) {
      if (now > tokens[i].expiresAt) {
        tokens.splice(i, 1);
      }
    }
  }
}
