import { Module } from '@nestjs/common';
import { TokenExpirationService } from './tokenExpiration.service';

@Module({
  providers: [TokenExpirationService]
})
export class TokenExpirationModule {}
