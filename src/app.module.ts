import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';

import { ApiModule } from '@api/api.module';

import { DatabaseModule } from '@modules/database/database.module';
import { ScheduleModule } from '@nestjs/schedule';
import { TokenExpirationModule } from './tasks/tokenExpiration.module';

@Module({
  imports: [
    ApiModule,
    DatabaseModule,
    ConfigModule.forRoot({ isGlobal: true }),
    ScheduleModule.forRoot(),
    TokenExpirationModule
  ]
})
export class AppModule {}
