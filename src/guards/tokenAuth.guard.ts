import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';

import { tokens } from '../tokens';

@Injectable()
export class TokenAuthGuard implements CanActivate {
  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();

    if (!tokens.find((token) => token.token === request.headers.token)) {
      return false;
    }

    return true;
  }
}
