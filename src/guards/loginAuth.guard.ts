import { AuthService } from '@api/auth/auth.service';
import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';

@Injectable()
export class LoginAuthGuard implements CanActivate {
  constructor(private authService: AuthService) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();

    const user = await this.authService.verifyUser(
      request.body.email,
      request.body.password
    );

    if (!user) {
      return false;
    }

    return true;
  }
}
