import { Body, Controller, Post, UseGuards } from '@nestjs/common';

import { LoginAuthGuard } from 'src/guards/loginAuth.guard';
import { v4 as uuidv4 } from 'uuid';

import { tokens } from '../../tokens';
import { AuthService } from './auth.service';

const tokenLifetime = 60 * 1000;

@Controller('auth/login')
export class AuthController {
  constructor(private authService: AuthService) {}
  @Post()
  @UseGuards(LoginAuthGuard)
  async login(@Body() body: any) {
    const token = uuidv4();

    tokens.push({ token: token, expiresAt: Date.now() + tokenLifetime });

    return token;
  }
}
