import { UserInterface } from '@entities/user/interfaces/user.interface';
import { UserRepository } from '@entities/user/user.repository';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(UserRepository)
    private userRepository: UserRepository
  ) {}

  async verifyUser(email: string, password: string): Promise<UserInterface> {
    return await this.userRepository.findOne({
      where: {
        email,
        password
      }
    });
  }
}
