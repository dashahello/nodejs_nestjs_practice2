import { UsersEntityModule } from '@entities/user/user.module';
import { UserRepository } from '@entities/user/user.repository';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthService } from './auth.service';

import { AuthController } from './auth.controller';

@Module({
  imports: [UsersEntityModule, TypeOrmModule.forFeature([UserRepository])],
  providers: [AuthService],
  controllers: [AuthController]
})
export class AuthApiModule {}
