import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { AuthApiModule } from './auth/auth.module';

import { UsersApiModule } from './users/users.module';
import { Logger } from '../middlewares/logger.middleware';

@Module({
  imports: [UsersApiModule, AuthApiModule]
})
export class ApiModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(Logger).forRoutes('*');
  }
}
