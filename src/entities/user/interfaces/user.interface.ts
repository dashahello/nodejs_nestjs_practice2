import { UserRoleEnum } from '@enums/users/roles';

export interface UserInterface {
  id?: number;
  name: string;
  email?: string;
  password: string;
  role: UserRoleEnum;
  createdAt: Date;
  updatedAt: Date;
}
