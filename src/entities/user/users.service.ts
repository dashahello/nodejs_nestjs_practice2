import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { UserRoleEnum } from '@enums/users/roles';

import { UserInterface } from './interfaces/user.interface';

import { UserRepository } from './user.repository';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(UserRepository)
    private userRepository: UserRepository
  ) {}

  async getUsers(page = 1, size = 10): Promise<UserInterface[]> {
    return await this.userRepository.find({
      skip: (page - 1) * size,
      take: size
    });
  }

  async getUserById(id: number): Promise<UserInterface> {
    return await this.userRepository.findOne({
      where: { id }
    });
  }

  async createUser(user: Partial<UserInterface>): Promise<UserInterface> {
    return await this.userRepository.save({
      name: user.name,
      role: UserRoleEnum[user.role],
      email: user.email,
      password: user.password
    });
  }

  async getUserByEmail(email: string): Promise<UserInterface> {
    return await this.userRepository.findByEmail(email);
  }

  async deleteUser(id: number): Promise<void> {
    await this.userRepository.delete(id);
  }
}
